<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "position".
 *
 * @property integer $id
 * @property integer $series_id
 * @property integer $type
 * @property integer $capacity
 * @property integer $box
 */
class Position extends \yii\db\ActiveRecord
{
    const TYPE_EDP = 1; /* Парфюмированная вода */
    const TYPE_EDT = 2; /* Туалетная вода */
    const TYPE_EDC = 3; /* Одеколон */
    const TYPE_DEO = 4; /* Дезодорант */
    
    const BOX_PRESENT = 0; /* Подарочная (обычная) */
    const BOX_TESTER = 1; /* Тестер */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['series_id', 'type', 'capacity', 'box'], 'required'],
            [['series_id', 'type', 'box'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'series_id' => 'Series ID',
            'type' => 'Type',
            'capacity' => 'Capacity',
            'box' => 'Box',
        ];
    }
    
    public function getSeries() {
        return $this->hasOne(Series::className(), ['id' => 'series_id']);
    }
    
    public static function getTypes() {
        return [
            Position::TYPE_EDP => "Парфюмированная вода (EDP)",
            Position::TYPE_EDT => "Туалетная вода (EDT)",
            Position::TYPE_EDC => "Одеколон (EDT)",
            Position::TYPE_DEO => "Дезодорант (DEO)",
        ];
    }
    
    public static function getBoxTypes() {
        return [
            Position::BOX_PRESENT => "Подарочная (обычная)",
            Position::BOX_TESTER => "Тестер",
        ];
    }
}

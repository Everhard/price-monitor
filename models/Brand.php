<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brand".
 *
 * @property integer $id
 * @property string $name
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    
    public function getSeries() {
        return $this->hasMany(Series::className(), ['brand_id' => 'id']);
    }
    
    public function getSeriesMen() {
        return $this->hasMany(Series::className(), ['brand_id' => 'id'])->where(['gender' => Series::TYPE_MEN]);
    }
    
    public function getSeriesWomen() {
        return $this->hasMany(Series::className(), ['brand_id' => 'id'])->where(['gender' => Series::TYPE_WOMEN]);
    }
    
    public function getSeriesUnisex() {
        return $this->hasMany(Series::className(), ['brand_id' => 'id'])->where(['gender' => Series::TYPE_UNISEX]);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "website".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 */
class Website extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'website';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
        ];
    }
    
    public function getParserClassname() {

        $parser_name = str_replace("http://", "", $this->url);
        $parser_name = str_replace("https://", "", $parser_name);
        $parser_name = str_replace("www.", "", $parser_name);
        
        list($parser_name) = explode(".", $parser_name);
        
        $parser_name = ucfirst($parser_name);
        
        return $parser_name;
    }
    
    public static function getCompetitorWebsites() {
        
        $websites = [];
        
        foreach(self::findAll(['own' => 0]) as $website) {
            $websites[$website->id] = $website->name;
        }
        
        return $websites;
    }
    
    public function importPrices() {
        /*
         * Get website import object:
         */
        $import_classname = "app\\imports\\".$this->getParserClassname();
        $import = new $import_classname();
        $import->import();
    }
}

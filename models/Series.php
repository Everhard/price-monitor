<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "series".
 *
 * @property integer $id
 * @property integer $brand_id
 * @property string $name
 * @property integer $gender
 */
class Series extends \yii\db\ActiveRecord
{
    const TYPE_MEN = 1;
    const TYPE_WOMEN = 2;
    const TYPE_UNISEX = 3;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'series';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'name', 'gender'], 'required'],
            [['brand_id', 'gender'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Brand ID',
            'name' => 'Name',
            'gender' => 'Gender',
        ];
    }
    
    public function getBrand() {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }
    
    public function getPositions() {
        return $this->hasMany(Position::className(), ['series_id' => 'id']);
    }
    
    public static function getTypes() {
        return [
            self::TYPE_MEN => "Мужская категория",
            self::TYPE_WOMEN => "Женская категория",
            self::TYPE_UNISEX => "Универсальая категория",
        ];
    }
    
    public function getOwnType() {
        return self::getTypes()[$this->gender];
    }
    
    public function getGenderIcon() {
        return $this->gender == self::TYPE_MEN ? '<i class="fa fa-mars" aria-hidden="true"></i>' : '<i class="fa fa-venus" aria-hidden="true"></i>';
    }
}

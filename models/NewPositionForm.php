<?php

namespace app\models;

use Yii;
use yii\base\Model;

class NewPositionForm extends Model
{
    public $seriesId;
    public $type;
    public $capacity;
    public $box;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['seriesId', 'type', 'capacity', 'box'], 'required'],
            [['capacity'], 'integer', 'min' => 1, 'max' => 1000],
        ];
    }
    
    public function attributeLabels() {
        return [
            "type" => "Тип духов",
            "capacity" => "Объем",
            "box" => "Упаковка",
        ];
    }
}
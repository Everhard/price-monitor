<?php

namespace app\models;

use Yii;
use yii\base\Model;

class NewSeriesForm extends Model
{
    public $seriesName;
    public $gender;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['seriesName', 'gender'], 'required'],
            [['gender'], 'integer'],
        ];
    }
    
    public function attributeLabels() {
        return [
            "seriesName" => 'Название серии',
        ];
    }
}

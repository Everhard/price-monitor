<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "price".
 *
 * @property integer $id
 * @property integer $position_id
 * @property integer $website_id
 * @property integer $price
 * @property string $id_data
 */
class Price extends \yii\db\ActiveRecord
{
    
    
    const ERROR_PRICE = -1;
    const OUT_OF_STOCK = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'website_id'], 'required'],
            [['position_id', 'website_id'], 'integer'],
            [['id_data'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_id' => 'Position ID',
            'website_id' => 'Website ID',
            'price' => 'Price',
            'id_data' => 'ID Data',
        ];
    }
    
    public function getPriceString() {
        switch($this->price) {
            case Price::ERROR_PRICE:
                return "Ошибка";
            case Price::OUT_OF_STOCK:
                return "Нет в наличии";
            default:
                return $this->price." грн.";
        }
    }
    
    public function updatePrice() {
        
        /*
         * Get website and position objects:
         */
        $website = Website::findOne($this->website_id);
        $position = Position::findOne($this->position_id);

        /*
         * Get website parser object:
         */
        $parser_classname = "app\\parsers\\".$website->getParserClassname();
        $parser = new $parser_classname($this->id_data);
        
        /*
         * Get current price:
         */
        $this->price = $parser->getPrice($position->type, $position->capacity, $position->box);
        
        /*
         * Log if error:
         */
        if ($this->price == self::ERROR_PRICE) {
            
            $type_string = Position::getTypes()[$position->type];
            $box_string = Position::getBoxTypes()[$position->box];
            
            $text = "Ошибка при получении цены! URL: $this->id_data".". Объем: ".$position->capacity." мл. Тип: $type_string. Упаковка: $box_string.";
            
            $error_message = new Log(["message" => $text, "date_time" => time()]);
            $error_message->save();
        }

        $this->save();
    }
    
    public function getWebsite() {
        return $this->hasOne(Website::className(), ['id' => 'website_id']);
    }
}
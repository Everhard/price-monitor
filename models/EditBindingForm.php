<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EditBindingForm extends Model
{
    public $price_id;
    public $website_id;
    public $id_data;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['website_id', 'id_data', 'price_id'], 'required'],
            [['website_id', 'price_id'], 'integer'],
            [['id_data'], 'url']
        ];
    }
    
    public function attributeLabels() {
        return [
            "website_id" => 'Сайт конкурента',
            "id_data" => 'Ссылка на товар (URL)',
        ];
    }
}

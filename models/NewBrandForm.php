<?php

namespace app\models;

use Yii;
use yii\base\Model;

class NewBrandForm extends Model
{
    public $brandName;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['brandName'], 'required'],
        ];
    }
    
    public function attributeLabels() {
        return [
            "brandName" => 'Название бренда',
        ];
    }
}

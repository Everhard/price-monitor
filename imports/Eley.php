<?php

namespace app\imports;

use app\models\Price;

class Eley {
    
    private $url = "http://eley.com.ua/api/export.php?action=prices&token=a9d43765c61caebd138f1b0dcee1b674";
    
    public function import() {

        if ($content = file_get_contents($this->url)) {
            if ($prices = json_decode($content)) {
                
                Price::updateAll(['own_purchase_price' => -1], 'website_id = 1');
                
                foreach ($prices as $article => $price) {
                    if ($position = Price::find()->where(["website_id" => 1, "own_id_data" => $article])->one()) {
                        $position->own_purchase_price = $price;
                        $position->save();
                    }
                }
            }
        }
    }
}
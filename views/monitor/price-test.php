<?php

use yii\helpers\Url;
use \app\models\Price;

/* @var $this yii\web\View */

$this->title = 'Проверка цены';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Проверка цены</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <p>Укажите артикул товара, чтобы проверить цену:</p>
        
        <form role="form" method="get">
            <div class="box-body">
                <div class="form-group">
                  <label>Артикул (код):</label>
                  <input type="hidden" name="r" value="monitor/price-test" />
                  <input name="article" type="number" class="form-control" placeholder="Например, 2531">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Проверить</button>
                </div>
            </div>
        </form>
        <?php if (isset($position_found) && !$position_found) { ?>
        <p class="alert alert-warning">Позиция с таким артикулом не найдена!</p>
        <?php } ?>
        <?php
        if (isset($websites)) {
        ?>
        <div class="row">
            <div class="col-sm-5">
                <p class="alert alert-info">
                    Артикул: <strong><?=$article?></strong><br />
                    Позиция: <?=$position?><br />
                    Тип: <?=$type?><br />
                    Объем: <?=$capacity?>
                </p>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <th>Сайт</th>
                        <th>Цена</th>
                        <th>Ссылка на товар</th>
                    </thead>
                    <tbody>
                    <?php
                    if (count($websites)) foreach($websites as $website) {
                        echo "<tr>
                                <td>$website[website]</td>
                                <td>$website[price]</td>
                                <td>$website[link]</td>
                              </tr>";
                    } else echo "<tr><td colspan='3' class='text-center'>Нет сайтов для мониторинга.</td></tr>";
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php } ?>
        
        
    </div><!-- /.box-body -->

</div><!-- /.box -->
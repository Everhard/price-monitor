<?php
use app\assets\AppAsset;

AppAsset::register($this);


/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;

$this->title = 'Вход в систему мониторинга';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition lockscreen">
<?php $this->beginBody() ?>
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="/"><b>Мониторинг</b>Цен</a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name">Eley.com.ua</div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="/img/bazilik.png" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials" action='/index.php?r=monitor/login' method="post">
      <div class="input-group">
        <input type="hidden" name="username" value="eley" />
        <input name='password' type="password" class="form-control" placeholder="пароль">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <div class="input-group-btn">
          <button type="submit" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
    Введите пароль, чтобы авторизироваться
  </div>
  <div class="lockscreen-footer text-center">
    Copyright &copy; 2016 <b><a href="http://monitor.eley.com.ua" class="text-black">monitor.eley.com.ua</a></b><br>
    Все права защищены
  </div>
</div>
<!-- /.center -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

use yii\helpers\Url;
use \app\models\Price;


/* @var $this yii\web\View */

$this->title = 'Система мониторинга и корректировки цен';
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Журнал ошибок</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-hover brands-list">
            <tbody>
                <tr>
                    <th>Описание ошибки</th>
                    <th>Время</th>                    
                </tr>
<?php
if (count($logs)) foreach($logs as $log) {
    echo "<tr>
            <td>$log->message</td>
            <td>".date("d.m.Y (H:i)", $log->date_time)."</td>
          </tr>";
} else echo "<tr><td colspan='2' class='text-center'>Ошибок не зафиксировано.</td></tr>";
?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-xs-6"></div>
            <div class="col-xs-6 text-right">
                <p><a href="<?php echo Url::to(['monitor/error-log', 'clear' => true]); ?>" class="btn btn-sm btn-success left-crop"><i class="fa fa-trash"></i> Очистить журнал</a></p>
            </div>
        </div>
    </div><!-- box-footer -->
</div><!-- /.box -->
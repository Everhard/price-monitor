<?php

use yii\helpers\Url;
use \app\models\Price;

/* @var $this yii\web\View */

$this->title = 'Система мониторинга и корректировки цен';
?>

<div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua-gradient">
        <div class="inner">
            <h3><?=$all_positions_count?></h3>

          <p>Позиций в системе</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="<?=Url::to(['monitor/catalogue']); ?>" class="small-box-footer">Перейти в каталог <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p>
          <strong>Отслеживаемые позиции:</strong>
        </p>

        <div class="progress-group">
          <span class="progress-text">MakeUp</span>
          <span class="progress-number"><b><?=$makeup_positions_count?></b>/<?=$all_positions_count?></span>

          <div class="progress sm">
            <div class="progress-bar progress-bar-aqua" style="width: <?=round($makeup_positions_count*100/$all_positions_count); ?>%"></div>
          </div>
        </div>
        <!-- /.progress-group -->
        <div class="progress-group">
          <span class="progress-text">Parfums</span>
          <span class="progress-number"><b><?=$parfums_positions_count?></b>/<?=$all_positions_count?></span>

          <div class="progress sm">
            <div class="progress-bar progress-bar-red" style="width: <?=round($parfums_positions_count*100/$all_positions_count); ?>%"></div>
          </div>
        </div>
        <!-- /.progress-group -->
        <div class="progress-group">
          <span class="progress-text">Lagrande</span>
          <span class="progress-number"><b><?=$lagrande_positions_count?></b>/<?=$all_positions_count?></span>

          <div class="progress sm">
            <div class="progress-bar progress-bar-green" style="width: <?=round($lagrande_positions_count*100/$all_positions_count); ?>%"></div>
          </div>
        </div>
        <!-- /.progress-group -->
        <div class="progress-group">
          <span class="progress-text">Parfumeria</span>
          <span class="progress-number"><b><?=$parfumeria_positions_count?></b>/<?=$all_positions_count?></span>

          <div class="progress sm">
            <div class="progress-bar progress-bar-yellow" style="width: <?=round($parfumeria_positions_count*100/$all_positions_count); ?>%"></div>
          </div>
        </div>
        <!-- /.progress-group -->
      </div>
</div>
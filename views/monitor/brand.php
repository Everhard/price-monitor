<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use app\models\Position;
use app\models\Series;

$this->title = $brand->name;
$this->params['breadcrumbs'][] = ['label' => 'Каталог продукции', 'url' => ['monitor/catalogue']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-5">
        <?php
        foreach (Series::getTypes() as $key => $category_type) {
            
            $collapsed_class = 'collapsed-box';
            $box_tool_sign = 'plus';
            
            switch ($key) {
                case Series::TYPE_MEN:
                    $series = $brand->seriesMen;
                    $box_class = "box-primary";
                    if ($type == Series::TYPE_MEN) { $collapsed_class = ''; $box_tool_sign = 'minus'; }
                    break;
                case Series::TYPE_WOMEN:
                    $series = $brand->seriesWomen;
                    $box_class = "box-danger";
                    if ($type == Series::TYPE_WOMEN) { $collapsed_class = ''; $box_tool_sign = 'minus'; }
                    break;
                case Series::TYPE_UNISEX:
                    $series = $brand->seriesUnisex;
                    $box_class = "box-warning";
                    if ($type == Series::TYPE_UNISEX) { $collapsed_class = ''; $box_tool_sign = 'minus'; }
                    break;
            }
        ?>
        <div class="box <?= $collapsed_class ?> <?= $box_class ?>">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $category_type ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-<?= $box_tool_sign ?>"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="series-table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Бренд</th>
                            <th>Наименование серии</th>
                            <th>К-во позиций</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($series)) foreach ($series as $a_series) {
                            echo "<tr data-series-id='$a_series->id'>
                                    <td>".$brand->name."</td>
                                    <td>".$a_series->name."</td>
                                    <td>".count($a_series->positions)."</td>
                                  </tr>";
                        } else {
                            echo "<tr><td colspan='3' class='text-center'>В эту категорию ни одна серия не была добавлена</td></tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="row">
                    <div class="col-xs-6">
                        <p>Всего серий: <?= count($brand->series) ?></p>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p><a class="btn btn-sm btn-primary left-crop new-series-window-button" data-toggle="modal" data-target="#add-series-modal" data-category="<?=$key?>"><i class="fa fa-plus"></i> Добавить серию</a></p>
                    </div>
                </div>
            </div><!-- box-footer -->
        </div><!-- /.box -->
        <?php } ?>

    </div>
    <div class="col-sm-7 positions-blocks" id="js-positions-blocks">
        <?php foreach ($brand->series as $a_series) { ?>
        <div id="series-<?= $a_series->id ?>" class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Позиции <?= "$brand->name $a_series->name" ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Тип</th>
                            <th>Объем</th>
                            <th>Упаковка</th>
                            <th class="label-primary">Eley.com.ua</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($a_series->positions as $position) {

                            $price = app\models\Price::findOne(["position_id" => $position->id, "website_id" => 1]);
                            
                            if (!$price) {
                                $price_article = "-";
                                $price_id = -1;
                            }
                            else {
                                $price_article = $price->own_id_data;
                                $price_id = $price->id;
                            }

                            echo "<tr>
                                    <td>".Position::getTypes()[$position->type]."</td>
                                    <td>".$position->capacity."</td>
                                    <td>".Position::getBoxTypes()[$position->box]."</td>
                                    <td class='product-article' data-id=''>
                                        <form>
                                            <input name='price-id' value='$price_id' type='hidden' />
                                            <input name='position-id' value='$position->id' type='hidden' />
                                            <input name='website-id' value='1' type='hidden' />
                                            <input name='price-article' type='input' value='$price_article' />
                                        </form>
                                        <span>$price_article</span>
                                    </td>
                                    <td><a target='_blank' href='".Url::to(["monitor/position-monitor", "position_id" => $position->id])."' class='btn btn-xs btn-success monitor'><span class='fa fa-refresh'></span></a></td>
                                    <td><a title='Удалить позицию' href='#' class='remove btn btn-xs btn-info' data-id='".$position->id."'><span class='fa fa-remove'></span></a></td>
                                  </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="row">
                    <div class="col-xs-6">
                        <p>Всего позиций: <?= count($a_series->positions) ?></p>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p><a class="btn btn-sm btn-primary left-crop" data-toggle="modal" data-target="#add-position-modal"><i class="fa fa-plus"></i> Добавить позицию</a></p>
                    </div>
                </div>
            </div><!-- box-footer -->
        </div><!-- /.box -->
        <?php } ?>

    </div>
</div>

<div id="add-series-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Добавление новой серии</h4>
      </div>
      <div class="modal-body">
        <?php
        $form = \yii\bootstrap\ActiveForm::begin();
        
        echo $form->field($new_series, "seriesName")->textInput();
        echo Html::activeHiddenInput($new_series, "gender");      
        
        $form->end();
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button id="add-new-series-button" type="button" class="btn btn-primary">Добавить</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="add-position-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Добавление новой позиции</h4>
      </div>
      <div class="modal-body">
        <?php
        $form = \yii\bootstrap\ActiveForm::begin([
            "action" => Url::to(["monitor/add-position"]),
        ]);
        
        echo $form->field($new_position, "seriesId")->hiddenInput()->label(false);
        
        echo $form->field($new_position, "type")->dropDownList(Position::getTypes(), ['prompt'=>'Выберите тип духов']);
        
        echo $form->field($new_position, "capacity")->textInput();
        
        echo $form->field($new_position, "box")->dropDownList(Position::getBoxTypes(), ['prompt'=>'Выберите упаковку']);
                
        $form->end();
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button id="add-new-position-button" type="button" class="btn btn-primary">Добавить</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
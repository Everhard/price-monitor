<?php
use yii\helpers\Url;
use app\models\Series;

echo "<tr>
        <td>
            <a href='".Url::to(['monitor/brand', 'id' => $model->id])."'>".$model->name."</a>
        </td>
        <td>
            <div class='category-type'>
                <a href='".Url::to(['monitor/brand', 'id' => $model->id, 'type' => Series::TYPE_MEN])."' class='label label-primary hvr-float-shadow'>Мужская</a>
                <a href='".Url::to(['monitor/brand', 'id' => $model->id, 'type' => Series::TYPE_WOMEN])."' class='label label-danger hvr-float-shadow'>Женская</a>
                <a href='".Url::to(['monitor/brand', 'id' => $model->id, 'type' => Series::TYPE_UNISEX])."' class='label label-warning hvr-float-shadow'>Универсальная</a>
            </div>
        </td>
        <td>".count($model->series)."</td>
      </tr>";
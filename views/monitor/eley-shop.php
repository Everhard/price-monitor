<?php

use yii\helpers\Url;
use \app\models\Price;

/* @var $this yii\web\View */

$this->title = 'Магазин ELEY';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Магазин ELEY</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <p>Здесь вы можете просмотреть список позиций сайта ELEY, которые не подключены к системе мониторинга цен.</p>
        <?php
        if (isset($positions)) { ?>
            
        <p class="alert alert-warning">К системе мониторига не подключено позиций: <strong><?=count((array)$positions)?></strong>.</p>
        
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th>Бренд</th>
                <th>Наименование</th>
                <th>Категория</th>
                <th>Код</th>
            </thead>
            <tbody>
            <?php
            foreach($positions as $code => $position_array) {
                echo "<tr>
                        <td>$position_array->brand</td>
                        <td>$position_array->name</td>
                        <td>$position_array->category</td>
                        <td>$code</td>
                      </tr>";
            }
            ?>
            </tbody>
        </table>
        
        <?php } else { ?>
            <p><a href="<?=Url::to(['monitor/eley-shop', 'get_positions' => true]) ?>" class="btn btn-sm btn-primary left-crop"><i class="fa fa-globe"></i> Показать позиции</a></p>
        <?php } ?>
    </div>
</div>
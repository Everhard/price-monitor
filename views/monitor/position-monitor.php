<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\models\Website;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Система мониторинга и корректировки цен';
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            Мониторинг позиции: 
            <?= $position->series->brand->name; ?> 
            <?= $position->series->name; ?>
            <?= $position->capacity; ?> ml
            <?= $position->series->getGenderIcon(); ?></h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-hover brands-list" id="js-prices-table">
            <tbody>
                <tr>
                    <th>Вебсайт</th>
                    <th>Цена</th>
                    <th>Привязка</th>
                    <th class="edit-price"></th>
                    <th class="remove-price"></th>
                </tr>
                <?php
                if ($prices) foreach($prices as $price) {
                    
                    $website_name = Website::getCompetitorWebsites()[$price->website_id];
                    
                    echo "
                        <tr>
                            <td class='website-name' data-website-id='$price->website_id'>$website_name</td>
                            <td>".$price->getPriceString()."</td>
                            <td class='binding-link'><a href='https://href.li/?$price->id_data'>$price->id_data</a></td>
                            <td><a data-toggle='modal' data-target='#edit-binding-modal' title='Редактировать источник' href='#' class='edit btn btn-xs btn-info js-edit-price' data-id='".$price->id."'><span class='fa fa-edit'></span></a></td>
                            <td><a title='Удалить источник' href='".Url::to(['monitor/position-monitor', 'position_id' => $position->id, 'delete' => $price->id])."' class='remove btn btn-xs btn-danger js-delete-price' data-id='".$price->id."'><span class='fa fa-remove'></span></a></td>
                        </tr>";
                } else {
                    echo "<tr><td colspan='5' class='text-center'>Ни одного источника не добавлено</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-xs-6">
                <p>Всего отслеживаемых сайтов: <strong><?=count($prices)?></strong></p>
            </div>
            <div class="col-xs-6 text-right">
                <p><a class="btn btn-sm btn-primary left-crop" data-toggle="modal" data-target="#add-binding-modal"><i class="fa fa-plus"></i> Добавить источник</a></p>
            </div>
        </div>
    </div><!-- box-footer -->
</div><!-- /.box -->

<div id="add-binding-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Добавление нового источника</h4>
      </div>
      <div class="modal-body">
        <?php
        $form = ActiveForm::begin();
        
        echo $form->field($new_binding, "website_id")->dropDownList(Website::getCompetitorWebsites(), ['prompt'=>'Выберите сайт конкурента']);
        echo $form->field($new_binding, "id_data")->textInput();
        echo Html::activeHiddenInput($new_binding, "position_id", ["value" => $position->id]); 
        
        $form->end();
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button id="add-binding-button" type="button" class="btn btn-primary">Добавить</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="edit-binding-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Изменение источника</h4>
      </div>
      <div class="modal-body">
        <?php
        $form = ActiveForm::begin();
        
        echo $form->field($edit_binding, "website_id")->dropDownList(Website::getCompetitorWebsites(), ['prompt'=>'Выберите сайт конкурента']);
        echo $form->field($edit_binding, "id_data")->textInput();
        echo Html::activeHiddenInput($edit_binding, "price_id", ["value" => '']);
        
        $form->end();
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button id="save-binding-button" type="button" class="btn btn-primary">Сохранить</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
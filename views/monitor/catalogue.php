<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Series;
use yii\widgets\ListView;

$this->title = 'Каталог продукции';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Бренды</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <?php $table_layout = '{summary}
            <table class="table table-bordered table-hover brands-list">
                <tbody>
                    <tr>
                        <th>Наименование бренда</th>
                        <th>Быстрый переход</th>
                        <th>Количество серий</th>
                    </tr>
                    {items}
                </tbody>
            </table>
            <div class="pages">{pager}</div>';

            echo ListView::widget([
                'dataProvider' => $brands,
                'layout' => $table_layout,
                'itemView' => '_brand-row',
            ]);

            ?>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-xs-6"></div>
            <div class="col-xs-6 text-right">
                <p><a class="btn btn-sm btn-primary left-crop" data-toggle="modal" data-target="#add-brand-modal"><i class="fa fa-plus"></i> Добавить бренд</a></p>
            </div>
        </div>
    </div><!-- box-footer -->
</div><!-- /.box -->

<div id="add-brand-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Добавление нового бренда</h4>
      </div>
      <div class="modal-body">
        <?php
        $form = \yii\bootstrap\ActiveForm::begin();
        
        echo $form->field($new_brand, "brandName")->textInput();
                
        $form->end();
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button id="add-new-brand-button" type="button" class="btn btn-primary">Добавить</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
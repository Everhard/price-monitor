<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-purple sidebar-mini <?php if (Yii::$app->controller->action->id == "brand") { ?>sidebar-collapse<?php } ?>">
<?php $this->beginBody() ?>

<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>С</b>М</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Система</b>Мониторинга</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="/img/bazilik.png" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Eley.com.ua</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="/img/bazilik.png" class="img-circle" alt="User Image">

                <p>
                  Eley.com.ua
                  <small>Интернет-магазин парфюмерии</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a data-method="post" href="<?=Url::to(['monitor/logout']); ?>" class="btn btn-default btn-flat">Выход</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/img/bazilik.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Eley.com.ua</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Онлайн</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Поиск...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
    <?php
    echo Menu::widget([
      'items' => [
          ['label' => 'Меню'],
          ['label' => '<i class="fa fa-home"></i> <span>Главная</span>', 'url' => ['monitor/index']],
          ['label' => '<i class="fa fa-registered"></i> <span>Каталог продукции</span>', 'url' => ['monitor/catalogue']],
          ['label' => '<i class="fa fa-shopping-cart"></i> <span>Магазин ELEY</span>', 'url' => ['monitor/eley-shop']],
          ['label' => '<i class="fa fa-money"></i> <span>Проверка цены</span>', 'url' => ['monitor/price-test']],
          ['label' => '<i class="fa fa-check-square-o"></i> <span>Журнал ошибок</span>', 'url' => ['monitor/error-log']],
      ],
      'options' => [
          'class' => 'sidebar-menu',
      ],
      'encodeLabels' => false,
      'firstItemCssClass' => 'header',
    ]);

    ?>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Система мониторинга
        <small>и корректировки цен</small>
      </h1>
      <ol class="breadcrumb" style="display: none;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
      <?php
      echo Breadcrumbs::widget([
          'tag' => 'ol',
          'homeLink' => ['label' => '<i class="fa fa-dashboard"></i> Главная', 'url' => ['monitor/index']],
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          'encodeLabels' => false,
      ]);
      ?>
    </section>

    <!-- Main content -->
    <section class="content">
      
        <?php if(Yii::$app->session->hasFlash("success")): ?>
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <p><i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash("success"); ?></p>
        </div>  
        <?php endif; ?>
        
        <?php if(Yii::$app->session->hasFlash("error")): ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <p><i class="icon fa fa-exclamation"></i> <?= Yii::$app->session->getFlash("error"); ?></p>
        </div>  
        <?php endif; ?>
      
        <?= $content ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Система мониторинга цен
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="http://eley.com.ua">Eley.com.ua</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

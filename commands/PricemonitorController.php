<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Price;
use app\models\Website;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PricemonitorController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {
        echo "Help:
-> pricemonitor/start - to start process.
-> pricemonitor/import - to start import prices from Bazilik.com.ua.
-> pricemonitor/correct - to correct prices.
-> pricemonitor/push - to push prices to shop.
";
        
    }
    
    public function actionStart()
    {
        echo "Started!" ."\n";
        
        $prices = Price::find()->where(["website_id" => $this->getCompetitorWebsitesIDs()])->all();

        foreach($prices as $price) {
            $price->updatePrice();
        }
    }
    
    public function actionImport()
    {
        echo "Import started!" ."\n";
        
        $website = Website::findOne("1");
        $website->importPrices();
    }
    
    public function actionCorrect()
    {
        echo "Correct started!" ."\n";
        
        $usd_rate = 27.027;
        $minimum_profit = 100; // UAH
        $default_profit = 140; // UAH
        
        $prices = Price::find()->where("website_id = :website_id AND own_purchase_price != :own_purchase_price", ["website_id" => 1, "own_purchase_price" => -1])->all();
        
        foreach($prices as $price) {
            
            $competitor_min_price = 0;
            $default_price = ceil($price->own_purchase_price * $usd_rate + $default_profit);
            $min_acceptable_price = ceil($price->own_purchase_price * $usd_rate + $minimum_profit);
            
            $competitors = Price::find()->
                    where("website_id != :website_id AND position_id = :position_id AND price > :price", ["website_id" => 1, "position_id" => $price->position_id, "price" => $price->price])->
                    all();
            
            if ($competitors) {
                foreach ($competitors as $competitor) {

                    if ($competitor_min_price == 0) {
                        $competitor_min_price = $competitor->price;
                    }

                    if ($competitor->price < $competitor_min_price) {
                        $competitor_min_price = $competitor->price;
                    }
                }
                
                $our_price = $competitor_min_price - 25;
            } else {
                $our_price = $default_price;
            }

            
            
            if ($our_price < $min_acceptable_price) {
                $our_price = $min_acceptable_price;
            }
            
            $price->price = $our_price;
            $price->save();
        }
    }
    
    public function getCompetitorWebsitesIDs()
    {
        $websites = Website::find()->where(["own" => 0])->all();
        
        $website_ids = [];
        
        foreach ($websites as $website) {
            $website_ids[] = $website['id'];
        }
        
        return $website_ids;
    }
    
    public function actionPush()
    {
        echo file_get_contents("http://eley.com.ua/api/import.php?token=a9d43765c61caebd138f1b0dcee1b674");
    }
}


class HTTPClient {
	/*
		HTTPClient (cURL) Settings:
		*. CURLOPT_URL:              The URL to fetch.
		*. CURLOPT_USERAGENT:        The contents of the "User-Agent" header to be used in a HTTP request.
		*. CURLOPT_HEADER:           TRUE to include the header in the output.
		*. CURLOPT_TIMEOUT:          The maximum number of seconds to allow cURL functions to execute.
		*. CURLOPT_CONNECTTIMEOUT:   The number of seconds to wait while trying to connect.
		*. CURLOPT_POST:             TRUE to do a regular HTTP POST.
		*. CURLOPT_POSTFIELDS:       The full data to post in a HTTP "POST" operation.
		*. CURLOPT_COOKIEFILE:       The name of the file containing the cookie data.
		*. CURLOPT_RETURNTRANSFER:   TRUE to return the transfer as a string instead of outputting it out directly.
		*. CURLOPT_FOLLOWLOCATION:   TRUE to follow any "Location: " header that the server sends as part of the HTTP header.
		
		Docs on PHP.net:
		http://www.php.net/manual/ru/function.curl-setopt.php
	*/
	
	// Default parameters:
	const DEFAULT_TIMEOUT = 120;
	const DEFAULT_CONNCECT_TIMEOUT = 120;
	const DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0";

	public function __construct($url = NULL) {

		// Initialization cURL:
		$this->curl = curl_init($url);
		
		// Default cURL settings:
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->curl, CURLOPT_HEADER, FALSE);
		curl_setopt($this->curl, CURLOPT_TIMEOUT, self::DEFAULT_TIMEOUT);
		curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_CONNCECT_TIMEOUT);
		curl_setopt($this->curl, CURLOPT_USERAGENT, self::DEFAULT_USER_AGENT);
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, '');
	}

	public function do_request() {
		$result = curl_exec($this->curl);
		if (!$result) {
			$this->curl_errors[] = curl_error($this->curl)." (".curl_errno($this->curl).").";
			return false;
		}
		return $result;
	}

	public function set_url($url) {
		$this->url = $url;
		curl_setopt($this->curl, CURLOPT_URL, $this->url); 
	}
	
	public function set_user_agent($user_agent) {
		$this->user_agent = $user_agent;
		curl_setopt($this->curl, CURLOPT_USERAGENT, $this->user_agent);
	}
	
	public function set_post_method($post_data) {
		curl_setopt($this->curl, CURLOPT_POST, TRUE);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $post_data);
	}
	
	public function set_get_method() {
		curl_setopt($this->curl, CURLOPT_HTTPGET, TRUE);
	}
	
	public function close_client() {
		curl_close($this->curl);
	}

	public function has_errors() {
		return $curl_errors ? true : false;
	}
	
	private $url;
	private $user_agent;
	private $curl;
	private $curl_errors = array();
}
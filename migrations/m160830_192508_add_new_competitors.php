<?php

use yii\db\Migration;

class m160830_192508_add_new_competitors extends Migration
{
    public function up()
    {
        $this->insert("website", [
            "name" => "Parfums.ua",
            "url" => "https://parfums.ua",
            "own" => 0,
        ]);
        
        $this->insert("website", [
            "name" => "Parfumeria.ua",
            "url" => "http://www.parfumeria.ua",
            "own" => 0,
        ]);
        
        $this->insert("website", [
            "name" => "Kislinka.com.ua",
            "url" => "http://kislinka.com.ua",
            "own" => 0,
        ]);
        
        $this->insert("website", [
            "name" => "Aromat.kiev.ua",
            "url" => "http://aromat.kiev.ua",
            "own" => 0,
        ]);
    }

    public function down()
    {
        echo "m160830_192508_add_new_competitors cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161012_005048_new_log_field extends Migration
{
    public function up()
    {
        $this->addColumn('log', 'date_time', 'INT NOT NULL AFTER message');
    }

    public function down()
    {
        echo "m161012_005048_new_log_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

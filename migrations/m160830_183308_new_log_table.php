<?php

use yii\db\Migration;

class m160830_183308_new_log_table extends Migration
{
    public function up()
    {
        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'message' => $this->string()->notNull(),
        ]);
    }

    public function down()
    {
        echo "m160830_183308_new_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

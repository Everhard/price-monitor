$(document).ready(function() {
    $("#series-table tbody tr").click(function() {
        
        $("#series-table tbody tr").removeClass("label-info");
        $(this).addClass("label-info");
        
        var seriesId = $(this).data('series-id');
        $("div[id^=series-]").hide();
        
        $("#series-" + seriesId).fadeIn('100');
        
        $("#newpositionform-seriesid").val(seriesId);
        
//        $.ajax({
//            url: '/index.php?r=monitor/positions&series_id=' + seriesId,
//            method: 'post',
//            data: {
//                _csrf: $('meta[name=csrf-token]').attr("content"),
//            },
//            success: function() {
//                alert("ok");
//            }
//        });
    });
    
    /*
     * Add new brand:
     */
    $("#add-new-brand-button").click(function() {
        $("#add-brand-modal form").submit();
    });
    
    /*
     * Add new series:
     */
    $("#add-new-series-button").click(function() {
        $("#add-series-modal form").submit();
    });
    
    /*
     * Add new binding:
     */
    $("#add-binding-button").click(function() {
        $("#add-binding-modal form").submit();
    });
    
    /*
     * Add new position:
     */
    $("#add-position-modal form").on("beforeSubmit", function() {
        $("#add-position-modal form").ajaxSubmit({
            success: function(response) {
                var responseJson = eval("(" + response + ")");
                if (!responseJson.error) {
                    positionRow(responseJson.param);
                    $("#add-position-modal").modal('hide');
                    $("#add-position-modal form")[0].reset();
                } else {
                    alert("Ошибка! Возможно, такая позиция уже есть в списке.");
                }
            }
        });
        return false;
    })

    $("#add-new-position-button").click(function() {
        $("#add-position-modal form").submit();
    });
    
    /*
     * Remove position:
     */
    $("div[id^=series-] .remove").on("click", function() {
        console.log("remove button");
    });
    
    /*
     * New series:
     */
    $(".new-series-window-button").click(function() {
        var category = $(this).data("category");
        $("#newseriesform-gender").val(category);
    });
    
    /*
     * Fixed positions container:
     */
    if ($("#js-positions-blocks").length != 0) {
        
        var marginFromTop = $(".main-header").innerHeight() + $(".content-header").innerHeight() + 5;
        
        $(document).on("scroll", function() {
            
            var documentScroll = $(this).scrollTop();
            var currentWidth = $("#js-positions-blocks").innerWidth();
            
            if (documentScroll > marginFromTop) {
              $("#js-positions-blocks").addClass("fixed").css({
                  "width" : currentWidth
              });
            } else {
              $("#js-positions-blocks").removeClass("fixed").removeAttr("style");
            }
            
        })
    }
    
    /*
     * Positions monitor page:
     */
    $(".js-edit-price").on("click", function() {
        var priceId = $(this).data("id");
        var priceRow = $(this).parents("tr");
        var websiteId = priceRow.find("[data-website-id]").data("website-id");
        var link = priceRow.find(".binding-link a").text();
        
        $("#editbindingform-website_id option[value=" + websiteId + "]").prop('selected', true);
        $("#editbindingform-id_data").val(link);
        $("#editbindingform-price_id").val(priceId);
    });
    
    $(".js-delete-price").on("click", function(e) {
        if (!confirm("Вы уверены, что хотите удалить данную привязку?")) {
            e.preventDefault();
        }
    });
    
    $("#save-binding-button").on("click", function() {
        $("#edit-binding-modal form").submit();
    });
    
    $(".product-article").on("click", function() {
        $(this).find("span").hide();
        $(this).find("form").show().find("input[name=price-article]").focus();
    });
    
    $(".product-article form").ajaxForm({
        url: '/index.php?r=monitor/change-article',
        type: 'POST',
        success: function(response, statusText, xhr, form) {
            
            var responseJSON = eval("(" + response + ")");
            
            if (responseJSON.error == 0) {
                $(form).hide();
                $(form).next("span").text(responseJSON.priceArticle);
                $(form).next("span").show();
                
                $(form).find("input[name=price-id]").val(responseJSON.priceId);
            }
        }
    });
});

function positionRow(param) {
    
    this.html = "<tr>";
    this.html += "<td>" + param.type + "</td>";
    this.html += "<td>" + param.capacity + "</td>";
    this.html += "<td>" + param.box + "</td>";
    this.html += "<td>-</td>";
    this.html += "<td><a target='_blank' href='/index.php?r=monitor/position-monitor&position_id=" + param.id + "' class='btn btn-xs btn-success monitor'><span class='fa fa-refresh'></span> Мониторинг</a></td>";
    this.html += "<td><a title='Удалить позицию' href='#' data-id='" + param.id + "' class='btn btn-xs btn-info remove'><span class='fa fa-remove'></span></a></td>";
    this.html += "</tr>";
    
    $("#series-" + param.seriesId + " tbody").append(this.html);
}
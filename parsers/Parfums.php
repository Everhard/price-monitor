<?php

namespace app\parsers;
use Sunra\PhpSimple\HtmlDomParser;
use app\models\Price;
use app\models\Position;

class Parfums extends Parser {
    
    private $url;
    
    public function __construct($url) {
        $this->url = $url;
    }
    
    public function getPrice($type, $capacity, $box) {
        
        $current_price = Price::ERROR_PRICE;
        
        /*
         * Query to search:
         */
        switch ($type) {
            case Position::TYPE_EDT:
                $type_string = "туалетная вода";
                break;
            case Position::TYPE_EDP:
                $type_string = "парфюмированная вода";
                break;
            default:
                $type_string = "";
        }
        $capacity_string = $capacity." мл";
        $full_type_string = $box == Position::BOX_TESTER ? "тестер ($type_string)" : $type_string;
        $search_string = "$full_type_string $capacity_string";
        
        try {
            echo "Trying: $this->url | ";
            $page_content = HtmlDomParser::file_get_html($this->url);
        } catch (\Exception $ex) {
            $page_content = false;
        }
        
        if ($page_content) {
            
            $parsed_price = false;
            
            try {
                foreach ($page_content->find(".product-map .product-list tbody tr") as $tr) {
                    
                    /*
                     * Check if it's not header (compatibility with PHP 5):
                     */
                    if ($tr->find(".product-name .name")) {
                        $position_name = trim($tr->find(".product-name .name", 0)->plaintext);
                        if ($position_name == $search_string) {
                            if (!$tr->find("td.not-available")) {
                                $parsed_price = trim($tr->find("td.product-price span[itemprop=price]", 0)->plaintext);
                            } else {
                                $parsed_price = Price::OUT_OF_STOCK;
                            }
                            break;
                        }
                    }
                }

            } catch (\Exception $ex) {
                echo $ex;
            }

            if ($parsed_price !== false) {
                $current_price = $parsed_price;
            }
            
            if ($parsed_price) {
                echo "Got price: $parsed_price.";
            } else {
                echo "No price.";
            }
            
            echo "\n";
        }
        
        return $current_price;
    }
}
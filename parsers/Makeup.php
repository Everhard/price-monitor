<?php

namespace app\parsers;
use Sunra\PhpSimple\HtmlDomParser;
use app\models\Price;

class Makeup extends Parser {
    
    private $url;
    
    public function __construct($url) {
        $this->url = $url;
    }
    
    public function getPrice($type, $capacity, $box) {
        
        $current_price = Price::ERROR_PRICE;
        
        try {
            echo "Trying: $this->url | ";
            $page_content = HtmlDomParser::file_get_html($this->url);
        } catch (\Exception $ex) {
            $page_content = false;
        }
        
        if ($page_content) {
            
            try {
                $parsed_price = false;
                
                if ($page_content->find(".product-item__row .variant[title=".$capacity."ml]")) {
                    $parsed_price = $page_content->find(".product-item__row .variant[title=".$capacity."ml]", 0)->{"data-price"};
                }
                
                if (!$parsed_price) {
                    
                    $enabled_string = $page_content->find("#product_enabled", 0)->plaintext;
                    
                    if ($enabled_string == "Нет в наличии") {
                        $current_price = Price::OUT_OF_STOCK;
                    } else {
                        // If there are some other variants of position:
                        if ($page_content->find(".product-item__row .product-item__volume-radio")) {
                            $current_price = Price::OUT_OF_STOCK;
                        }
                    }
                }
                
                
            } catch (\Exception $ex) {
                $parsed_price = false;
                echo $ex;
            }
            
            if ($parsed_price) {
                echo "Got price: $parsed_price.";
                $current_price = $parsed_price;
            } else {
                echo "No price.";
            }
            
            echo "\n";
        }

        return $current_price;
    }
}
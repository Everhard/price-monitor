<?php

namespace app\parsers;

abstract class Parser {
    
    abstract public function __construct($url);
    abstract public function getPrice($type, $capacity, $box);
    
}
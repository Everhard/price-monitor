<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use yii\data\ActiveDataProvider;

use app\models\LoginForm;
use app\models\NewBrandForm;
use app\models\NewSeriesForm;
use app\models\NewPositionForm;
use app\models\NewBindingForm;
use app\models\EditBindingForm;

use app\models\Price;
use app\models\Website;
use app\models\Brand;
use app\models\Series;
use app\models\Position;
use app\models\Log;


class MonitorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login', 'index', 'catalogue', 'brand', 'add-position', 'position-monitor', 'error-log', 'export'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'catalogue', 'brand', 'add-position', 'position-monitor', 'error-log', 'export'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'export'],
                        'allow' => true,
                        'roles' => ['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $all_positions_count = count(Price::find()->where(["website_id" => 1])->all());
        $makeup_positions_count = count(Price::find()->where(["website_id" => 2])->all());
        $parfums_positions_count = count(Price::find()->where(["website_id" => 4])->all());
        $lagrande_positions_count = count(Price::find()->where(["website_id" => 3])->all());
        $parfumeria_positions_count = count(Price::find()->where(["website_id" => 5])->all());
        
        return $this->render('index', [
            "all_positions_count" => $all_positions_count,
            "makeup_positions_count" => $makeup_positions_count,
            "parfums_positions_count" => $parfums_positions_count,
            "lagrande_positions_count" => $lagrande_positions_count,
            "parfumeria_positions_count" => $parfumeria_positions_count,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->login()) {
            return $this->goBack();
        }
        return $this->renderPartial('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionCatalogue()
    {
        $new_brand = new NewBrandForm();
        if ($new_brand->load(Yii::$app->request->post()) && $new_brand->validate()) {
            if (!Brand::findOne(["name" => $new_brand->brandName])) {
                $brand = new Brand([
                    "name" => $new_brand->brandName,
                ]);
                $brand->save();
                Yii::$app->getSession()->setFlash('success', 'Бренд <strong>'.$brand->name.'</strong> был успешно добавлен в систему!');
                return $this->refresh();
            }
            
        }
        
        $brands = new ActiveDataProvider([
            'query' => Brand::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        return $this->render('catalogue', [
            'brands' => $brands,
            'new_brand' => $new_brand,
        ]);
    }
    
    public function actionBrand($id, $type = Series::TYPE_MEN)
    {
        $new_series = new NewSeriesForm();
        if ($new_series->load(Yii::$app->request->post()) && $new_series->validate()) {
            if (!Series::findOne(["name" => $new_series->seriesName, "brand_id" => $id, "gender" => $new_series->gender])) {
                $series = new Series([
                    "brand_id" => $id,
                    "name" => $new_series->seriesName,
                    "gender" => $new_series->gender,
                ]);
                $series->save();
                Yii::$app->getSession()->setFlash('success', 'Серия <strong>'.$series->name.'</strong> была успешно добавлена в систему!');
                return $this->refresh();
            }
            
        }
        
        $new_position = new NewPositionForm();
        
        $brand = Brand::findOne($id);
        
        return $this->render('brand', [
            'brand' => $brand,
            'new_series' => $new_series,
            'new_position' => $new_position,
            'type' => $type,
        ]);
    }
    
    public function actionAddPosition() {
        if (Yii::$app->request->isAjax) {
            
            $message['error'] = 1;
            
            $new_position = new NewPositionForm();
            if ($new_position->load(Yii::$app->request->post()) && $new_position->validate()) {
                if (!Position::findOne(["series_id" => $new_position->seriesId, "type" => $new_position->type, "capacity" => $new_position->capacity, "box" => $new_position->box])) {
                    $position = new Position([
                        "series_id" => $new_position->seriesId,
                        "type" => $new_position->type,
                        "capacity" => $new_position->capacity,
                        "box" => $new_position->box,
                    ]);
                    
                    if ($position->save()) {
                        $message['error'] = 0;
                        $message['param'] = [
                            "seriesId" => $new_position->seriesId,
                            "id" => $position->id,
                            "type" => Position::getTypes()[$new_position->type],
                            "capacity" => $new_position->capacity,
                            "box" => Position::getBoxTypes()[$new_position->box],
                        ];
                    }
                }
            }
            
            echo json_encode($message);
        }
    }
    
    public function actionChangeArticle() {
        if (Yii::$app->request->isAjax) {
            
            $response['error'] = 1;
            
            $params = Yii::$app->request->post();
            if (!empty($params['price-id']) && !empty($params['price-article']) && !empty($params['website-id']) && !empty($params['position-id'])) {
                if (is_numeric($params['price-id']) && is_numeric($params['price-article']) && is_numeric($params['website-id']) && is_numeric($params['position-id'])) {

                    /*
                     *  New price:
                     */
                    if ($params['price-id'] == -1) {
                        
                        $price = new Price([
                            "position_id" => $params['position-id'],
                            "website_id" => $params['website-id'],
                            "own_id_data" => $params['price-article'],
                        ]);
                        
                        $result = $price->save();
                        
                    } else {
                        if ($price = Price::find()->where(['id' => $params['price-id'], 'website_id' => $params['website-id']])->one()) {
                            $price->own_id_data = $params['price-article'];
                            $result = $price->save();
                        }
                    }
                    
                    if ($result) {
                        $response['error'] = 0;
                        $response['priceId'] = $price->id;
                        $response['priceArticle'] = $price->own_id_data;
                    }
                    
                    
                }
            }
            
            echo json_encode($response);
        }
    }
    
    public function actionPositionMonitor($position_id, $delete = false) {
        
        /*
         * Delete price:
         */
        if ($delete) {
            if ($price = Price::findOne($delete)) {
                if ($price->delete()) {
                    return $this->redirect(Url::to(['monitor/position-monitor', 'position_id' => $position_id]));
                }
            }
        }
        
        /*
         * Add new binding:
         */
        $new_binding = new NewBindingForm();
        if ($new_binding->load(Yii::$app->request->post()) && $new_binding->validate()) {
            if (!Price::findOne(["position_id" => $new_binding->position_id, "website_id" => $new_binding->website_id])) {
                $price = new Price([
                    "position_id" => $new_binding->position_id,
                    "website_id" => $new_binding->website_id,
                    "id_data" => $new_binding->id_data,
                ]);
                $price->save();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Данный веб-сайт уже имеется в списке!');
            }
        }
        
        /*
         * Edit existing binding:
         */
        $edit_binding = new EditBindingForm();
        if ($edit_binding->load(Yii::$app->request->post()) && $edit_binding->validate()) {
            if ($price = Price::findOne($edit_binding->price_id)) {
                
                $price->website_id = $edit_binding->website_id;
                $price->id_data = $edit_binding->id_data;
                
                $price->save();
            }
        }
        
        $position = Position::findOne($position_id);
        $prices = Price::find()
                ->where(["position_id" => $position_id])
                ->andWhere(['!=', 'website_id', '1'])
                ->all();
        
        return $this->render("position-monitor", [
            "position" => $position,
            "new_binding" => $new_binding,
            "edit_binding" => $edit_binding,
            "prices" => $prices,
        ]);
    }
    
    public function actionEleyShop($get_positions = false) {
        
        if ($get_positions) {
            if ($content = file_get_contents("http://eley.com.ua/api/export.php?action=positions-on-stock&token=a9d43765c61caebd138f1b0dcee1b674")) {
                if ($positions = json_decode($content)) {

                    $exist_positions_codes = [];
                    $prices = Price::find()->where(["website_id" => 1])->all();
                    foreach ($prices as $price) {
                        $exist_positions_codes[] = $price->own_id_data;
                    }
                    
                    $positions_array = [];
                    foreach ($positions as $key => $position) {
                        if (!in_array($key, $exist_positions_codes)) {
                            $positions_array[$key] = $position;
                        }
                    }

                    
                    return $this->render("eley-shop", [
                        "positions" => $positions_array,
                    ]);
                }
            }
        }
        
        return $this->render("eley-shop");
    }
    
    public function actionPriceTest($article = false) {
        
        if ($article) {
            
            $position_found = false;
            
            if ($price_search = Price::find()->where(['own_id_data' => $article])->one()) {
                
                $position = Position::findOne($price_search->position_id);
                
                $websites = [];

                if ($price = Price::find()->where("own_id_data = :own_id_data", ["own_id_data" => $article])->one()) {
                    foreach(Price::find()->where(['position_id' => $price->position_id])->all() as $price) {
                        $websites[] = [
                            "website" => $price->website->name,
                            "price" => $price->getPriceString(),
                            "price_int" => $price->price,
                            "link" => $price->id_data ? "<a href='".$price->id_data."'>Перейти</a>" : '-',
                        ];
                    }
                    
                    usort($websites, function($a, $b) {
                        if ($a['price_int'] == $b['price_int']) {
                            return 0;
                        }
                        return ($a['price_int'] > $b['price_int']) ? -1 : 1;
                    });
                    
                    // Tip:
                    $last = "";
                    foreach ($websites as $key => $website) {
                        if ($website['website'] == "Eley.com.ua" && $last && $website['price'] > 0) {
                            $difference = $last - $websites[$key]['price'];
                            $websites[$key]['price'] .= " <span class='text-success'>(-$difference грн)</span>";
                        }
                        
                        $last = $website['price'];
                    }
                }

                $position_type_string = Position::getTypes()[$position->type];

                return $this->render("price-test", [
                    "websites" => $websites,
                    "position" => "{$position->series->brand->name} {$position->series->name} {$position->series->getGenderIcon()}",
                    "capacity" => "{$position->capacity} мл",
                    "type" => $position_type_string,
                    "article" => $article,

                ]); 

                $position_found = true;
            }
            
            return $this->render("price-test", [
                "position_found" => $position_found,
            ]);
            
        }
        
        return $this->render("price-test");
    }
    
    public function actionErrorLog($clear = false) {
        
        if ($clear) {
            Log::deleteAll();
            return $this->redirect(Url::to(['monitor/error-log']));
        }
        
        $logs = Log::find()->all();
        
        return $this->render("error-log", [
            "logs" => $logs,
        ]);
    }
    
    public function actionExport() {
        
        $pricelist = [];
        
        $prices = Price::find()->where("website_id = :website_id AND own_purchase_price != :own_purchase_price", ["website_id" => 1, "own_purchase_price" => -1])->all();
        
        foreach ($prices as $price) {
            $pricelist[$price->own_id_data] = $price->price;
        }
        
        echo json_encode($pricelist);
    }
}